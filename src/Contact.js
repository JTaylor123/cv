import React from 'react'
import "./Contact.css" 
import stamp from "./assets/stamp.jpg"
import postage from "./assets/postagemark.jpg"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faPaperPlane } from "@fortawesome/free-solid-svg-icons"

export default function Contact() {
  return (
    
    <div className="contact">
        <h1>Get in touch</h1>
        
        <form className="contact-form">
            <div className="stamp"> 
                <img src={postage}/>
                <img src={stamp}/>
                
            </div>
            <p className="greeting">Dear Julia:</p>
            <label htmlFor='message'></label>
            <textarea rows={5} className="input" id='message' placeholder='Message'  />
            <p className="greeting">From:</p>
            <label htmlFor='name'></label>
            <input className="input" id='name' type="text" placeholder='Name' />
            <label htmlFor='company'></label>
            <input className="input" id='company' type="text" placeholder='Company' />
            <label htmlFor='email'></label>
            <input className="input" id='email' type="email" placeholder='Email'  />
            

            <button className="form-button" type="submit"> <FontAwesomeIcon icon={faPaperPlane} /></button>
        </form>
    </div>
    

  )
}
