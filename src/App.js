import Navbar from './Navbar';
import Homepage from './Homepage';
import Education from './Education';
import Keyskills from './Keyskills';
import Experience from './Experience';
import Contact from './Contact';

function App() {

  //return <Accordian />
  return (
    <div className="App">
      <Navbar />
     <div className="content">
       <Homepage />
       <Education />
       <Keyskills />
       <Experience />
       <Contact />
     </div>
    </div>
  );
}

export default App;
