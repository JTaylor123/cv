import "./Keyskills.css";
import Skill from './Skill';

export default function Keyskills() {
  return (
    <ul className='skills'>
    <Skill skill='Team Player'/>
    <Skill skill='Time Managment'/>
    <Skill skill='Hard-Working & Motivated'/>
    <Skill skill='Outstanding Numerical Skills'/>
    <Skill skill='Analytically Minded'/>
    <Skill skill='Determined Problem Solver'/>
    <Skill skill='Adaptable & Flexible'/>
    <Skill skill='Attention to Detail'/>
    <Skill skill='Great Communicator'/>
    <Skill skill='Eager to Learn'/>
    <Skill skill='Work under pressure'/>
    <Skill skill='Organised - Efficient & Productive'/>
    <Skill skill='Positive Influencer'/>
    <Skill skill='Commerically Driven'/>
    <Skill skill='Basic HTML5, CSS, ReactJS, Javascript, Python'/>
    <Skill skill='Advanced SQL and VBA'/>
    <Skill skill='Advanced Microsoft Office'/>
    <Skill skill='Specialist programs e.g. Dbeaver, Visual Studio Code'/>
    </ul>
  )
}

