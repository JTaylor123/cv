import Card, { CardBody } from './Card'
import "./Education.css" 
import Uni from "./assets/liverpool.png";
import College from "./assets/winstanley.jpg";
import School from "./assets/culcheth.png";

export const Education = () => {
  return (
    <div className="education">
      <Card img={Uni} daterange='2014 - 2017'>
        <CardBody>
              <div className="results">
              <p>BSc (hons) Chemistry, 2:1</p>
              </div>
              <div>
                  <ul className="details">
                      <li>Inorganic Chemistry</li>
                      <li>Organic Chemistry</li>
                      <li>Physical Chemistry</li>
                      <li>Spectroscopy</li>
                      <li>Key Skills (Maths for Chemists)</li>
                      <li> Inorganic Applications of Group Theory</li>
                      <li>Chemistry for Sustainable Technologies</li>
                      <li>Chemical Database Skills</li>
                      <li>Chemical Engineering for Chemists</li>
                  </ul>
              </div>
        </CardBody>
      </Card>
      <Card img={College} daterange='2012 - 2014'>
      <ul className="results2">
        <li>A2 Chemistry - B</li>
        <li>A2 Maths - B</li>
        <li>A2 History - B</li>
        <li>AS Extended Project - A*</li>
        <li>AS Biology - D</li>
        
        </ul>
      </Card>
      <Card img={School} daterange='2007 - 2012'>
      <CardBody>
        <div className="results2">
        <p>12 GSCEs grades A*-C including:
          <ul>
            <li>Maths - A</li>
            <li>English Language - A*</li>
            <li>French - B</li>
          </ul>
          </p>
        </div>
      </CardBody>
      </Card>
    </div>
    
  )
}
export default Education