import MyPhoto from "./assets/pic.jpeg";
import "./Homepage.css"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import {faMapLocationDot} from "@fortawesome/free-solid-svg-icons"

const Homepage = () => {
    return (
        <div className="homepage">
            <img src={MyPhoto} className="circle" />
            <p><FontAwesomeIcon icon={faMapLocationDot} /> Manchester</p>
            <div className="statement">
                <p>I am a hard-working, motivated individual looking to move into software development. I am driven by challenge, using teamwork and my analytical skills to find solutions. 
                </p>
            </div>
        </div>
        
        
    );
}

export default Homepage