import "./Card.css"

export function CardBody(props) {
    return (
        <div className="card-body">
            {props.children}
        </div>
    )
}

export default function Card(props) {
  return (
    <div className="card-container">
        <div className="image-container">
            <img src={props.img} className="card-image"/>
        </div>
        <div className="card-sub">
            <p>{props.daterange}</p>
        </div>
        {props.children}
    </div>
  )
}








