import "./Experience.css" ;
import  Accordion, {AccordionItem} from "./Accordion.js";

export default function Experience() {
  return (
    <div className="experience">
        <h1 className="experience-header">Employment History</h1>
        <Accordion>
          <AccordionItem title={<p><strong>HSS Hire</strong> Data Analyst - October 2020-Present</p>}>
            <ul>
              <li>Work independently within a small Data team in the Machine & Tool hire industry</li>
              <li>Strong ability to create and run SQL (postgres) and Access (Ingres) queries</li>
              <li>Strong Excel skills (incl. vlookups, pivots, macros)</li>
              <li>Extract and manipulate commercial data from company databases (Brenda/Spanner)</li>
              <li>Regularly construct new reports for commercial insights to aid the sales and supply chain team</li>
              <li>Create Automated reports and exposure to OBIS for dashboards</li>
              <li>Liaise with other departments for their data needs</li>
              <li>Prepare financial reports and investigate anomalies for branches, regions & customers</li>
              <li>Work from the Freshdesk ticket system in order to help both internal/external enquiries e.g. revenue reports, customer invoicing, stock on hire, top 100 products </li>
              <li>Area of expertise focuses on supplier analysis, some special reports include:
                <ul>
                <li>Supplier Ultimate Report - supplier specific - includes revenue, contract, availability and response, potential revenue from rejections etc</li>
                <li>Supplier Dashboard - whole supplier library based on last 12 months spend, contracts, availability checks (for insights into which suppliers aren’t performing)</li>
                <li>Monthly Supplier Invoicing Reports by product, region, branch, product category</li>
                <li>Contract Margin Reports - loss/low margin</li>
                </ul>
              </li>
            </ul>
          </AccordionItem>
          <AccordionItem title={<p><strong>Envirolab</strong> Inorganic Analyst - March 2019-October 2020</p>}>
            <ul>
              <li>Work independently within a UKAS accredited inorganic lab that is responsible for routine soil, water and leachate analysis</li>
              <li>In-depth Aquakem experience (colorimetry):
                <ul>
                <li>Busy instrument - runs a very high volume of samples and fast turnaround</li>
                <li>Ability to troubleshoot instrument issues and keep the instrument maintained</li>
                <li>Extraction and analysis of data using Excel</li>
                <li>Responsible for training other analysts on this instrument</li>
                </ul>
              </li>
              <li>Competent in HPLC and TOC analysis and some exposure to ICP and MS analysis </li>
              <li>Use of LIMS to work off an outstanding work list and report results</li>
              <li>One of the internal auditors in the lab to keep SOPs up to date and make sure they are followed and find improvements</li>
              <li>Ability to produce spreadsheet macros to make data analysis more efficient</li>
              <li>Ability to find anomalies in a large dataset</li>
                      
            </ul>
          </AccordionItem>
          <AccordionItem title={<p><strong>IKEA Warrington</strong> Restaurant Co-worker - September 2014-March 2019</p>}>
            <ul>
              <li>Work independently within a UKAS accredited inorganic lab that is responsible for routine soil, water and leachate analysis</li>
              <li>In-depth Aquakem experience (colorimetry):
                <ul>
                <li>Busy instrument - runs a very high volume of samples and fast turnaround</li>
                <li>Ability to troubleshoot instrument issues and keep the instrument maintained</li>
                <li>Extraction and analysis of data using Excel</li>
                <li>Responsible for training other analysts on this instrument</li>
                </ul>
              </li>
              <li>Competent in HPLC and TOC analysis and some exposure to ICP and MS analysis </li>
              <li>Use of LIMS to work off an outstanding work list and report results</li>
              <li>One of the internal auditors in the lab to keep SOPs up to date and make sure they are followed and find improvements</li>
              <li>Ability to produce spreadsheet macros to make data analysis more efficient</li>
              <li>Ability to find anomalies in a large dataset</li>
                      
            </ul>
          </AccordionItem>
        </Accordion>
        <h1 className="experience-header">Other Experience</h1>
        <Accordion>
          <AccordionItem title={<p><strong>Woolston Youth Group</strong> 2013-present</p>}>
            <ul>
              <li>One of the leaders in the Youth Group for 10-16 year olds</li>
              <li>Various activities available including table tennis, pool, board games, music, arts and crafts, and sports and assist in the purchasing of stock for the tuck shop and counting the float</li>
              <li>DBS checked</li>
            </ul>
          </AccordionItem>
          <AccordionItem title={<p><strong>Liverpool Science Jamboree </strong> 2016</p>}>
            <ul>
              <li>Successfully coordinated and ran an outreach activity for many groups of Beavers, Brownies, Scouts and Guides in order to complete their Science Badge</li>
              <li>“Colour Chemistry” session was developed featuring ‘chemical chameleon’ reaction, flame tests and colour wheels</li>
              <li>Positive feedback from the kids and leaders - thoroughly enjoyed participating to inspire kids into being interested in science</li>
            </ul>
          </AccordionItem>
          <AccordionItem title={<p><strong>Young Enterprise</strong> 2013</p>}>
            <ul>
              <li>Created and ran a business in a group of 6  - bought and sold products, marketed the business, assisted with the finances - reached the Greater Manchester finals</li>
            </ul>
          </AccordionItem>
          <AccordionItem title={<p><strong>Warrington Hospital Pharmacy</strong> Aug 2013</p>}>
            <ul>
              <li>Experienced the different roles within a pharmacy – dispensing, stock management, aseptic, anticoagulation and shadowing a pharmacist when on wards</li>
            </ul>
          </AccordionItem>
        </Accordion>




    </div>
  )
}
