import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import {faCircleDown, faCircleUp } from "@fortawesome/free-solid-svg-icons"
import { useRef, useState } from "react"
import "./Accordion.css" ;
import classNames from "classnames";



export function AccordionItem(props) {
    const [isOpen,setIsOpen] = useState(false);
    const myRef = useRef(null);
    function onClick() {
        setIsOpen(!isOpen);
        myRef.current.scrollIntoView();
    }

  return (
    <div class="accordion-item">
        
    <button onClick={onClick} class="accordion-header" type="button" >
            {props.company}
            {props.title}
            {props.date}
        <FontAwesomeIcon icon={isOpen ? faCircleUp : faCircleDown} />
    </button>
    <div class={classNames("accordion-body", {open: isOpen})} ref={myRef}>
        {props.children}
    </div>
</div>
  )
}



export default function Accordian(props) {
    

  return (
    <div class="accordion" id="accordionPanelsStayOpenExample">  {props.children}  </div>
  )
}
